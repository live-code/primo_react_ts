import React from "react";
import {Link} from "react-router-dom";

export const NavBar: React.VFC = () => {
  return (
    <div>
      <Link to="/login">Login</Link>
      <Link to="/users">users</Link>
      <Link to="/contacts">contacts</Link>
    </div>
  )
}
