import React, { useContext, useState } from 'react';

// Simple Context that contains a number
const CountContext = React.createContext<number | null>(null);

// Context with data structure
interface RandomState {
  value: number | null,
}
const RandomContext = React.createContext<RandomState | null>(null);

export  function Demo3MultipleContext() {
  console.log('App: render')
  const [count, setCount] = useState<number>(0);
  const [random, setRandom] = useState<RandomState>({ value: 0});

  return (
    <div className="comp">
      <CountContext.Provider value={count}>
        <RandomContext.Provider value={random}>
          <h1>Demo B: Context</h1>
          <button onClick={() => setCount(count - 1)}>-</button>
          <button onClick={() => setCount(count + 1)}>+</button>
          <button onClick={() => setRandom({ value: Math.random()})}>Random</button>
          <Dashboard  />
        </RandomContext.Provider>
      </CountContext.Provider>
    </div>
  );
}

// Need React.memo to avoid useless renders
const Dashboard = React.memo(() => {
  console.log(' dashboard: render')

  return <div>
    <CounterPanel />
    <RandomPanel />
  </div>
})

function CounterPanel () {
  console.log('  Counter Panel: render')

  const state = useContext(CountContext);
  return <div className="comp">
    Count: {state}
  </div>
}
const RandomPanel = () => {
  console.log('  Random Panel: render')

  const state = useContext(RandomContext);
  return <div className="comp">
    RandomPanel: {state?.value}
  </div>
}
