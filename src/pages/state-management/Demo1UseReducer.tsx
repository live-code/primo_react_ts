import React, {Dispatch, SetStateAction, useReducer} from 'react';

interface AppState {
  count: number;
  random: number;
}

interface AppAction {
  type: AppActionType;
  payload?: any;
}

type AppActionType = 'inc' | 'dec' | 'setRandom'

function reducer(state: AppState, action: AppAction) {
  switch(action.type) {
    case 'inc': return {...state, count: state.count + action.payload }
    case 'dec': return {...state, count: state.count - 1 }
    case "setRandom": return {...state, random: Math.random() }
  }
  return state;
}

export  function Demo1UseReducer() {
  const [state, dispatch] = useReducer(reducer, { count: 0, random: 0.5})

  console.log(state)
  return (
    <div className="comp">
      <button onClick={() => dispatch({ type: 'dec'})}>-</button>
      <button onClick={() => dispatch({ type: 'inc', payload: 10})}>+</button>
      <button onClick={() => dispatch({ type: 'setRandom'})}>random</button>
      <Dashboard count={state.count} random={state.random} dispatch={dispatch}/>
    </div>
  );
}

interface DashboardProps {
  count: number,
  random: number,
  dispatch: Dispatch<AppAction>
}

const Dashboard: React.FC<DashboardProps> = props => {
  console.log(' Dashboard: render')
  return <div className="comp">
    Dashboard
    <CounterPanel value={props.count} dispatch={props.dispatch}/>
    <RandomPanel value={props.random} />
  </div>
}


interface CounterPanelProps {
  value: number;
  dispatch: Dispatch<SetStateAction<any>>
}

const CounterPanel: React.FC<CounterPanelProps> = ((props) => {
  console.log('  CounterPanel: render')
  return <div className="comp">
    Count: {props.value}
  </div>
})

const RandomPanel: React.FC<{ value: number }> = ((props) => {
  console.log('  Random Panel: render')
  return <div className="comp">
    Random Value: {props.value}
  </div>
})
