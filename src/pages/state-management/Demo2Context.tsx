import React, {useContext, useState} from 'react';

interface AppState {
  count: number
  random: number
}


const AppContext = React.createContext<AppState | null>(null);
const initialState = {count: 0, random: 0.5};

export default function Demo2Context() {
  console.log('------\nApp: render')
  const [value, setValue] = useState<AppState>(initialState)

  return (
    <AppContext.Provider value={value}>
      <h3>Demo Context</h3>
      <button onClick={() => setValue({ ...value, count: value.count + 1})}>+</button>
      <button>-</button>
      <button>Random</button>
      <Dashboard  />
    </AppContext.Provider>
  );
}

const Dashboard = React.memo(() => {
  console.log(' dashboard: render')
  return <div>
    <CounterPanel />
    <RandomPanel />
  </div>
})

const CounterPanel = () => {
  console.log('  CounterPanel: render')
  const obj = useContext(AppContext)
  return <div className="comp">
    Count: {obj?.count}
  </div>
}


const RandomPanel = () => {
  console.log('  Random Panel: render')
  const obj = useContext(AppContext)
  return <div className="comp">
    <pre> Random {obj?.random}</pre>
  </div>
}
