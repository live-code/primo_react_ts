import React, { useState } from 'react';

// ===== NEW ===== //
export default function Demo5Composition() {
  console.log('------\nApp: render')
  const [count, setCount] = useState<number>(0);
  const [random, setRandom] = useState<number>(0);

  return (
    <div className="comp">
      <h1>Demo Hooks: useState</h1>
      <button onClick={() => setCount(count - 1)}>-</button>
      <button onClick={() => setCount(count + 1)}>+</button>
      <button onClick={() => setRandom(Math.random())}>Random</button>

      <Dashboard>
        <CounterPanel value={count} />
        <RandomPanel value={random} />
      </Dashboard>
    </div>
  );
}

const Dashboard: React.FC = (props) => {
  console.log(' Dashboard: render')
  return <div className="comp">
    Dashboard
    {props.children}
  </div>
}

// ===== OLD ===== //
// Child Component
// BAD: rendered even when Random is updated and it's not necessary
function CounterPanel (props: { value: number }) {
  console.log('  CounterPanel: render')
  return <div className="comp">
    CounterPanel: {props.value}
  </div>
}

// Child Component
// BAD: rendered even when Count is updated and it's not necessary
const RandomPanel: React.FC<{ value: number }> = (props) => {
  console.log('  Random Panel: render')
  return <div className="comp">
    RandomPanel: {props.value}
  </div>
}


