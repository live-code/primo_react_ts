import React, { useContext, useReducer } from 'react';

interface AppState {
  count: number;
  random: number;
}

// NEW 1
type ActionType = 'increment' | 'decrement' | 'random';
type Action = { type: ActionType, payload?: AppState}

// NEW 2
function appReducer(state: AppState, action: Action) {
  switch (action.type) {
    case 'decrement': return ({...state, count: state.count - 1});
    case 'increment': return ({...state, count: state.count + 1});
    case 'random': return ({...state, random: Math.random()});
    default: return state;
    // default: throw new Error(`Unhandled action: ${action.type}`)
  }
}

// NEW 5
type Dispatch = (action: Action) => void;
const AppContext = React.createContext<AppState | undefined>( undefined)
const DispatchContext = React.createContext<Dispatch>(() => null);
// or
// const DispatchContext = React.createContext<Dispatch<Action>>(() => null);

// NEW 4
const initialState = { count: 1, random: 0};

export default function Demo4UseReducerContext() {
  console.log('App: render')
  // NEW 3
  const [state, dispatch] = useReducer(appReducer, initialState);

  // UPDATE 6
  return (
    <>
      <h3>Demo Hooks: Context & useReducer</h3>
      <AppContext.Provider value={state}>
        <DispatchContext.Provider value={dispatch}>
          <Dashboard />
        </DispatchContext.Provider>
      </AppContext.Provider>
    </>
  );
}

const Dashboard: React.FC = React.memo(props => {
  console.log(' Dashboard: render')
  return <div className="comp">
    <Buttons />
    <CounterPanel />
    <RandomPanel  />
  </div>
})

// UPDATE 7
function Buttons () {
  const dispatch = useContext(DispatchContext);
  // UPDATE 7B - you can now verify if context is updated
  // const state = useContext(AppContext);
  console.log('  Buttons: render')

  return <div className="comp">
    <button onClick={() => dispatch({ type: 'decrement' })}>-</button>
    <button onClick={() => dispatch({ type: 'increment' })}>+</button>
    <button onClick={() => dispatch({ type: 'random' })}>random</button>
  </div>;
}

// UPDATE 8
// BAD: rerendered even when random changes
function CounterPanel () {
  console.log('  CounterPanel: render')
  const state = useContext(AppContext);
  return <div className="comp">Count/Random: {state?.count} - { state?.random}</div>
}

const RandomPanel: React.FC = React.memo(() => {
  console.log('  Random Panel: render')
  // UPDATE 9: remove comment to see how this component
  // is now rendered each time context is updated
  // const state = useContext(AppContext);
  return <div className="comp">Random Value: ABC</div>
})
