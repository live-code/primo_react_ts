import React, {useCallback, useEffect, useMemo, useState} from "react";

const memory = new Set()

export const Demo1Performance = () => {
  const [value, setValue] = useState<number>(0);

  const setTitle = useCallback(() => {
    document.title = 'qualcosa...' + value
  }, [value]);

  memory.add(setTitle)
  console.log(memory)

  return (
    <div>
      <button onClick={() => setValue(s => s + 1)}> {value}</button>
      { [null, null].map((item, index) => <Panel key={index} onSetTitle={(val) => setValue(val)}/>)}
    </div>
  )
}



interface PanelProps {
  onSetTitle: (val: number) => void
}

export const Panel: React.FC<PanelProps> = React.memo((props) => {
  console.log('panel render')
  const [counter, setCounter] = useState<number>(0)
  return (
    <div>
      <button onClick={() => setCounter(c => c + 1)}>+</button>
      <button onClick={() => props.onSetTitle(123)}>Set Title</button>
      Panel
    </div>
  )
})
