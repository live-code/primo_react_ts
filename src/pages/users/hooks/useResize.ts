import {useEffect, useState} from "react";

export function useResize() {
  const [size, setSize] = useState({ w: 0, h: 0})

  useEffect(() => {
    let timer: number;

    const fn = () => {
      clearTimeout(timer);
      timer = window.setTimeout(() => {
        console.log('timer')
        setSize({ w: window.innerWidth, h: window.innerHeight})
      }, 500)
    };

    window.addEventListener('resize', fn)

    return () => {
      window.removeEventListener('resize', fn)
    }
  }, [])

  return {
    w: size.w,
    h: size.h
  }
}
