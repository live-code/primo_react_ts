import {useEffect, useState} from "react";
import {User} from "../UsersPage";
import axios from "axios";

const INITIAL_STATE = { name: '', phone: ''};

export function useUsers() {
  const [error, setError] = useState<boolean>(false);
  const [users, setUsers] = useState<User[]>([]);
  const [selectedUser, setSelectedUser] = useState<Partial<User>>(INITIAL_STATE)

  useEffect(() => {
    axios.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .then((res) => {
        setUsers(res.data)
      })
      .catch(e => setError(true))

    return () => {
      console.log('hoook destroy')
    }
  }, [])


  function onsubmitHandler(user: Partial<User>) {
    if (user.id) {
      editUser(user)
    } else {
      addUser(user)
    }
  }

  function editUser(user: Partial<User>) {
    axios.patch<User>('https://jsonplaceholder.typicode.com/users/' + user.id, user)
      .then(res => {
        const newUsers = users.map(u => {
          return u.id === selectedUser.id ? res.data : u;
        })
        setUsers(newUsers)
      })
  }

  function addUser(user: Partial<User>) {
    axios.post<User>('https://jsonplaceholder.typicode.com/users', user)
      .then(res => {
        setUsers([...users, res.data])
      })
  }

  function reset() {
    setSelectedUser({...INITIAL_STATE})
  }

  function onSelectHandler(u: User) {
    setSelectedUser(u)
  }


  function deleteUser(idToDelete: number) {
    setError(false);
    axios.delete(`https://jsonplaceholder.typicode.com/users/${idToDelete}`)
      .then(() => {
        setUsers(users.filter(u => u.id !== idToDelete))
        reset()
      })
      .catch(e => setError(true))
  }

  return {
    error,
    selectedUser,
    users,
    actions: {
      onSubmit: onsubmitHandler,
      reset,
      onSelectHandler,
      deleteUser,
    }
  }
}


