import React, {useEffect, useState} from 'react';
import axios from "axios";
import {UsersErrors} from "./components/UsersErrors";
import {UsersList} from "./components/UsersList";
import {UsersForm} from "./components/UsersForm";
import {useResize} from "./hooks/useResize";
import {useUsers} from "./hooks/userUsers";

export interface User {
  id: number;
  name: string;
  phone: string;
}

export function UsersPage() {
  const { w, h } = useResize()
  const {
    selectedUser, users, error, actions
  } = useUsers();


  useEffect(() => {
    return () => {
      console.log('compo destroy')
    }
  }, [])


  return (
    <div>
      {w} - {h}
      <UsersErrors err={error} />

      <UsersForm
        selectedUser={selectedUser}
        onSubmit={actions.onSubmit}
        onReset={actions.reset}
      />

      <UsersList
        users={users}
        activeUserId={selectedUser.id}
        onSelectUser={actions.onSelectHandler}
        onDeleteUser={actions.deleteUser}
      />
    </div>
  );
}
