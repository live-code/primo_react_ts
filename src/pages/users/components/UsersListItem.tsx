import React, {useState} from "react";
import cn from "classnames";
import {User} from "../UsersPage";

interface UsersListItemProps {
  user: User;
  selected: boolean;
  onSelectUser: (user: User) => void
  onDeleteUser: (id: number) => void
}

export const UsersListItem: React.VFC<UsersListItemProps> = ({
  user, selected, onDeleteUser, onSelectUser
}) => {
  const [open, setOpened] = useState<boolean>(false)

  function deleteHandler(id: number, e: React.MouseEvent<HTMLButtonElement>) {
    e.stopPropagation();
    onDeleteUser(id)
  }

  function toggleHandler(e: React.MouseEvent<HTMLButtonElement>) {
    e.stopPropagation();
    setOpened(!open)
  }

  return (
    <li
      className={cn(
        'border-b p-3',
        {'bg-pink-300': selected}
      )}
       onClick={() => onSelectUser(user)}>
      {user.name}
      <button onClick={(e) => deleteHandler(user.id, e)} className="bg-pink-500"> Delete</button>
      <button onClick={e => toggleHandler(e)}>Toggle</button>

      {open && (<div>
        body bla bla bla
      </div>)}
    </li>
  )
}
