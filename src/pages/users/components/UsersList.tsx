import React, {useState} from "react";
import {User} from "../UsersPage";
import cn from "classnames";
import {UsersListItem} from "./UsersListItem";

interface UsersListProps {
  users: User[];
  activeUserId: number | undefined;
  onSelectUser: (user: User) => void
  onDeleteUser: (id: number) => void
}
export const UsersList: React.VFC<UsersListProps> = (props) => {
  return <>
    {
      props.users.map(u =>
        <UsersListItem
          key={u.id}
          user={u}
          selected={u.id === props.activeUserId}
          onSelectUser={props.onSelectUser}
          onDeleteUser={props.onDeleteUser}
        />
      )

    }
  </>
}
