import React, {useEffect, useState} from "react";
import {User} from "../UsersPage";

const INITIAL_STATE = { name: '', phone: ''};


interface UsersFormProps {
  onSubmit: (user: Partial<User>) => void;
  selectedUser: Partial<User>;
  onReset: () => void;
}
export const UsersForm: React.VFC<UsersFormProps>  = ({
  onSubmit, onReset, selectedUser
}) => {

  const [formData, setFormData] = useState<Partial<User>>(INITIAL_STATE)

  useEffect(() => {
    setFormData(selectedUser)
  }, [selectedUser])

  function onsubmitHandler(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
    onSubmit(formData)
    onReset()
  }

  function onChangeHandler(event: React.ChangeEvent<HTMLInputElement>) {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value
    })
  }

  const isNameValid = formData.name && formData.name.length > 3;
  const isPhoneValid = formData.phone && formData.phone.length > 3;
  const isFormValid: any = isNameValid && isPhoneValid;


  return (

    <form onSubmit={onsubmitHandler} >
      <input type="text" value={formData.name} name="name"
             onChange={onChangeHandler} placeholder="name" />
      <input type="text" value={formData.phone} name="phone"
             onChange={onChangeHandler} placeholder="phone" />
      <button type="button" onClick={onReset}>RESET </button>
      <button type="submit" disabled={!isFormValid}>SUBMIT {JSON.stringify(isFormValid)} </button>
    </form>
  )
}
