import create from "zustand";
import axios from "axios";
import {useEffect} from "react";

type Theme = 'light' | 'dark';

export interface AppState {
  counter: number;
  users: any[],
  theme: Theme;
  changeTheme: (th: Theme) => void;
  write: () => void;
  getUsers: () => void;
}

export const useStore = create<AppState>((set, get) => ({
  counter: 0,
  theme: 'light',
  users: [],
  changeTheme: (th: Theme) => set(s => ({ ...s, theme: th})),
  inc: () => set(s => ({...s, counter: s.counter + 1})),
  write: () => {
    window.localStorage.setITem('item', get().theme)
  },
  getUsers: async function() {
    const response = await axios.get('https://jsonplaceholder.typicode.com/users')
    set(s => ({ ...s, users: response.data }))
  }

}))


export function ContactsPage() {
  const theme = useStore(state => state.theme);
  const changeTHeme = useStore(state => state.changeTheme);
  const getUsers = useStore(state => state.getUsers);

  useEffect(() => {
    getUsers()
  }, [])

  return <div>
    <h1>Contacts {theme}</h1>
    <button onClick={() => changeTHeme('dark')}>Change Theme</button>
    <Panel />
  </div>
}


function Panel() {
  const users = useStore(state => state.users);
  const theme = useStore(state => state.theme);
  return <div>{theme} - {users.length}</div>

}

