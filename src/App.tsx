import React, {useState} from 'react';
import {BrowserRouter, Navigate, Route, Routes} from 'react-router-dom';
import {LoginPage} from "./pages/login/LoginPage";
import {UsersPage} from "./pages/users/UsersPage";
import {ContactsPage} from "./pages/contacts/ContactsPage";
import {NavBar} from "./core/components/NavBar";
import {Demo1Performance} from "./pages/demo1-performance/Demo1Performance";
import {Demo1UseReducer} from "./pages/state-management/Demo1UseReducer";
import Demo2Context from "./pages/state-management/Demo2Context";
import {Demo3MultipleContext} from "./pages/state-management/Demo3MultipleContext";
import Demo4UseReducerContext from "./pages/state-management/Demo4UseReducerContext";
import Demo5Composition from "./pages/state-management/Demo5Composition";


function App() {

  return (
    <BrowserRouter>
      <NavBar />
      <RoutesConfig />
    </BrowserRouter>

  );
}

export default App;


// route config
function RoutesConfig() {
  return (
    <Routes>
      <Route path="login" element={<LoginPage /> } />
      <Route path="users" element={<UsersPage /> } />
      <Route path="contacts" element={<ContactsPage /> } />
      <Route path="demo-performance" element={<Demo1Performance /> } />
      <Route path="demo-use-reducer" element={<Demo1UseReducer /> } />
      <Route path="demo-use-context" element={<Demo2Context /> } />
      <Route path="demo-use3-context" element={<Demo3MultipleContext /> } />
      <Route path="demo-use4-context" element={<Demo4UseReducerContext /> } />
      <Route path="demo-use5-context" element={<Demo5Composition /> } />
      <Route path="/" element={<Navigate to="users" />} />
    </Routes>
  )
}
